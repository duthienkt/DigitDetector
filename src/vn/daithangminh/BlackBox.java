package vn.daithangminh;

import processing.core.PApplet;

import java.applet.Applet;
import java.io.*;
import java.util.Scanner;

/**
 * Created by DuThien on 03/01/2017.
 */
public class BlackBox {

    FeaturedItem items[];
    int [] res;
    private int n;

    public BlackBox() {
        n = 0;
        items = new FeaturedItem[15000];
        res = new int [15000];
    }

    public void learn(FeaturedItem i, int result)
    {
        res[n] = result;
        items[n++] = i;
    }

    public boolean exist(FeaturedItem it)
    {
        for (int i = 0; i< n; ++i)
            if (items[i].equals(it)) return true;
        return false;
    }

    public int query(FeaturedItem item)
    {
        float m = -1;
        int result = 0;
        for (int i = 0; i < n; ++i) {
            float t = items[i].isMatch(item);
            if ( t>= m) {
                m = t;
                result = res[i];
            }
        }
        //PApplet.println(m+ " "+ result);
        return result;
    }

    public boolean save(String path) {
        OutputStream out;
        try {
            out = new FileOutputStream(path);
            PrintWriter writer = new PrintWriter(out);
            writer.println(n);
            for (int i = 0; i < n; ++i) {
                writer.print(res[i]+" ");
                items[i].onExport(writer);
                writer.flush();
            }
            writer.flush();
            out.close();

        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public static BlackBox load(PApplet applet, String path)
    {
        BlackBox res = new BlackBox();
        InputStream in;
        try {
            in = new FileInputStream(path);
            Scanner scanner = new Scanner(in);
            res.n = scanner.nextInt();
            for(int i = 0; i< res.n; ++i){
                res.res[i] = scanner.nextInt();
                res.items[i] = FeaturedItem.onImport(applet, scanner);}
            in.close();
        }
        catch (Exception e)
        {
            return res;
        }
        return res;

    }
}
