package vn.daithangminh;

import processing.core.PApplet;
import processing.core.PConstants;
import processing.core.PGraphics;
import processing.core.PImage;

import java.io.PrintWriter;
import java.util.Scanner;

/**
 * Created by DuThien on 02/01/2017.
 */
public class FeaturedItem {
    public static final int cW = 6;
    public static final int cH = 9;
    public static final int vecL = cW*cH;
    public static final int pW = cW * 8 + 2;
    public static final int pH = cH * 8 + 2;
    final float threshold = 100;

    final float match = 1.0f / vecL;
    final float ignore = match/20;
    PGraphics g;
    PApplet applet;

    private int data[] = new int[vecL];

    public FeaturedItem(PApplet applet, PImage image) {
        this.applet = applet;
        g = applet.createGraphics(pW, pH);
        g.beginDraw();
        g.image(image, 0, 0, pW, pH);
        g.filter(PConstants.BLUR);
        g.endDraw();
        create();
    }

    @Override
    public boolean equals(Object obj) {
        FeaturedItem it = (FeaturedItem) obj;
        for (int i = 0; i< vecL;++i)
            if (data[i]!= it.data[i]) return false;
        return true;
    }

    public FeaturedItem(PApplet applet, PImage _image, Rect r) {
        this.applet = applet;
        PImage image = _image.get(r.left, r.top, r.right - r.left + 1, r.button - r.top + 1);
        g = applet.createGraphics(pW, pH);
        g.beginDraw();
        g.image(image, 0, 0, pW, pH);
        g.endDraw();
        create();
    }

    private FeaturedItem(PApplet applet) {
        this.applet = applet;

    }

    public static FeaturedItem onImport(PApplet applet, Scanner scanner) {
        FeaturedItem res = new FeaturedItem(applet);
        for (int i = 0; i < vecL; ++i)
            res.data[i] = scanner.nextInt();
        return res;

    }

    private void create() {

        for (int i = 0; i < cH; ++i)
            for (int j = 0; j < cW; ++j) {
                int indx = i * cW + j;
                HeapMax heap = new HeapMax(100);
                for (int x = j * 8 + 1; x < (j + 1) * 8 ; ++x)
                    for (int y = i * 8 + 1; y < (i + 1) * 8 ; ++y) {
                        Vec2 vec = new Vec2(
                                applet.brightness(g.get(x + 1, y)) - applet.brightness(g.get(x - 1, y)),
                                applet.brightness(g.get(x, y + 1)) - applet.brightness(g.get(x, y - 1)));
                        //PApplet.println(vec.length());
                        heap.push(vec
                        );
                    }
                Vec2 sum = new Vec2(0, 0);
                for (int k = 0; k < 7; ++k) {
                    Vec2 vec = heap.pop();
                    sum = sum.increase(vec);
                }
                //PApplet.println(sum.length());
                if (sum.length() > threshold)
                    data[indx] = sum.getDegreeId();
                else data[indx] = -1;
            }


    }

    public void virtualization(float _x, float _y) {
        applet.stroke(0, 255, 0);
        for (int i = 0; i < cH; ++i)
            for (int j = 0; j < cW; ++j) {
                int indx = i * cW + j;
                if (data[indx] < 0) continue;
                float e = data[indx] * applet.PI / 9 - applet.PI / 2;
                float dx = applet.cos(e) * 5;
                float dy = applet.sin(e) * 5;

                applet.line(_x + j * 10 - dx, _y + i * 10 - dy, _x + j * 10 + dx, _y + i * 10 + dy);

            }
    }

    public PGraphics getImage() {
        return g;
    }

    public float isMatch(FeaturedItem item) {
        if (equals(item)) return 2;
        float res = 0;
        for (int i = 0; i < vecL; ++i)
            if (item.data[i] == data[i]) res += match;
            else {
                if (item.data[i] < 0 || data[i] < 0) res += ignore;
                else
                    res += Vec2.dot(item.data[i], data[i]) * match;
            }
        return res;
    }

    public void onExport(PrintWriter writer) {
        for (int i = 0; i < vecL; ++i)
            writer.print(data[i] + " ");
        writer.println();
    }
}
