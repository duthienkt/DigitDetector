package vn.daithangminh;

/**
 * Created by DuThien on 02/01/2017.
 */
public class HeapMax {
    public final int LIMIT_N;
    public Vec2[] data;
    private int n;

    public HeapMax(int limitData) {
        LIMIT_N = limitData;
        data = new Vec2[limitData];
        n = 0;
    }

    public int getN() {
        return n;
    }

    private void upHeap(int i) {
        if (i == 0) return;
        int pr = (i - 1) / 2;
        if (data[pr].length() >= data[i].length()) return;
        swap(pr, i);
        upHeap(pr);
    }


    private void downHeap(int i) {
        int child = i * 2 + 1;
        if (child + 1 < n)
            if (data[child].length() < data[child + 1].length()) child++;
        if (data[i].length() >= data[child].length()) return;
        swap(i, child);
        downHeap(child);
    }

    private void swap(int i, int j) {
        Vec2 t = data[i];
        data[i] = data[j];
        data[j] = t;
    }

    public void push(Vec2 v) {
        if (n >= LIMIT_N) n--;
        data[n++] = v;
        upHeap(n - 1);
    }

    public Vec2 pop()
    {
        Vec2 res = data[0];
        swap(0, n-1);
        n--;
        return res;
    }
}
