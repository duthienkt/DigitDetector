package vn.daithangminh;

import processing.core.PApplet;
import processing.core.PGraphics;
import processing.core.PImage;
import vn.daithangminh.minicv.BlackDetect;
import vn.daithangminh.minicv.Histogram;

import java.io.File;
import java.util.List;

/**
 * Created by DuThien on 03/01/2017.
 */
public class MainTest extends PApplet {
    float threshold = 100;
    int SQ_min = 28;


    PImage preview = null;
    PImage input = null;
    BlackBox blackBox;

    List<Rect> rects = null;
    FeaturedItem aChar[] = null;
    int[] detected = null;
    int[] dx = new int[]{0, 1, 1, 1, 0, -1, -1, -1};
    int[] dy = new int[]{-1, -1, 0, 1, 1, 1, 0, -1};
    int savedCount = 0;

    public static void main(String[] args) {
        // write your code here
        PApplet.main(new String[]{MainTest.class.getName()});
    }

    @Override
    public void settings() {
        super.settings();
        size(1200, 700);
    }

    public void selectFile() {
        selectInput("Select a file to process:", "fileSelected");
    }

    public void fileSelected(File selection) {
        if (selection == null) {
            println("Window was closed or the user hit cancel.");
        } else {
            println("User selected " + selection.getAbsolutePath());
            PImage image = loadImage(selection.getAbsolutePath());
            if (image == null) return;
            if (image.width * image.height <= 0) return;
            onImageSelected(loadImage(selection.getAbsolutePath()));
        }
    }

    void onImageSelected(PImage image) {

        preview = image;
        Histogram histogram = Histogram.create(this, image);
        println(histogram.getThreshold(0.5f));
        if (histogram.dataS[histogram.getThreshold(0.5f)] > 0.5) {
            input = image.copy();
            Histogram.reverse(this, input);
        } else
            input = image;
        rects = null;
        detected = null;
        aChar = null;
    }

    void detect() {
        print("detect() ");
        rects = BlackDetect.detectRect(this, input, 12, 100);
        println(" complete");
    }

    void classify() {
        print("classify()");
        aChar = new FeaturedItem[rects.size()];
        int p = 0;
        detected = new int[rects.size()];
        for (int i = 0; i < rects.size(); ++i) {
            aChar[i] = new FeaturedItem(this, input, rects.get(i));
            detected[i] = blackBox.query(aChar[i]);
            if (i * 10 / rects.size() > p) {
                p += 1;
                print(p * 10 + "% ");
            }
        }
        println("100% complete");


    }

    void testBlackBox() {
        int count = 0;
        int pass = 0;
        for (int f = 0; f < 10; ++f) {
            String folder = f + "/out/";
            int i = 0;
            while (true) {
                PImage im = loadImage(folder + i + ".jpg");
                if (im == null) break;
                if (im.width == 0) break;
                if (blackBox.query(new FeaturedItem(this, im))== f) pass++;
                count++;
                ++i;
            }
        }
        println("pass " + pass + " fail " + (count - pass));
    }

    @Override
    public void setup() {
        super.setup();
        blackBox = blackBox.load(this, "BLE.txt");
        //testBlackBox();
        //exit();
        selectFile();
    }

    @Override
    public void draw() {
        background(255);
        float sx = 1;
        float sy = 1;

        pushMatrix();
        if (input != null) {
            sx = width / (float) input.width;
            sy = height / (float) input.height;
            if (sx > sy) sx = sy;
            scale(sx);
            image(preview, 0, 0);
            if (rects == null) {
                if (frameCount % 3 == 2) detect();
                return;
            }
            for (int i = 0; i < rects.size(); ++i) {
                noFill();
                strokeWeight(2);
                stroke(255, 0, 0);
                Rect r = rects.get(i);
                rect(r.left - 2, r.top - 2, r.right - r.left + 1 + 2, r.button - r.top + 1 + 2);
            }
            if (detected == null) {
                if (frameCount % 3 == 2) classify();
                return;
            }

            for (int i = 0; i < rects.size(); ++i) {
                fill(0, 24, 255);
                Rect r = rects.get(i);
                textSize(17);
                text("[" + detected[i] + "]", r.left + 5, r.top + 5);
            }

        }
        popMatrix();
        if (aChar != null)
            for (int i = 0; i < rects.size(); ++i) {
                Rect r = rects.get(i);
                if (new Rect((int) (mouseX / sx), (int) (mouseY / sx), (int) (mouseX / sx), (int) (mouseY / sx)).isOverlap(r)) {
                    aChar[i].virtualization(mouseX, mouseY);
                    break;
                }
            }
    }

    @Override
    public void mouseClicked() {
        saveRes();
        selectFile();

    }

    void saveRes() {
        if (detected == null) return;
        PGraphics gr = createGraphics(input.width, input.height);
        gr.beginDraw();
        gr.image(input, 0, 0);
        for (int i = 0; i < rects.size(); ++i) {
            gr.noFill();
            gr.strokeWeight(2);
            gr.stroke(255, 0, 0);
            Rect r = rects.get(i);
            gr.rect(r.left - 2, r.top - 2, r.right - r.left + 1 + 2, r.button - r.top + 1 + 2);
        }
        for (int i = 0; i < rects.size(); ++i) {
            gr.fill(0, 24, 255);
            Rect r = rects.get(i);
            gr.textSize(17);
            gr.text("[" + detected[i] + "]", r.left + 5, r.top + 5);
        }
        gr.endDraw();
        gr.save((++savedCount) + ".jpg");
    }
}
