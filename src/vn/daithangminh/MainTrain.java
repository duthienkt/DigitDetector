package vn.daithangminh;

import processing.core.PApplet;
import processing.core.PImage;
import vn.daithangminh.minicv.BlackDetect;
import vn.daithangminh.minicv.Histogram;

import java.util.List;

public class MainTrain extends PApplet {

    PImage trainData;
    FeaturedItem aChar[];
    int count = 0;
    List<Rect> rects;
    BlackBox blackBox;
    int[] dx = new int[]{0, 1, 1, 1, 0, -1, -1, -1};
    int[] dy = new int[]{-1, -1, 0, 1, 1, 1, 0, -1};
    float threshold = 100;
    int SQ_min = 8;
    int id = 0;

    public static void main(String[] args) {
        // write your code here
        PApplet.main(new String[]{MainTrain.class.getName()});
    }

    @Override
    public void settings() {
        super.settings();
        size(1200, 730);
    }

    @Override
    public void setup() {
        super.setup();
        trainData = loadImage("train.jpg");
        rects = BlackDetect.detectRect(this, trainData, 10, 100);
        aChar = new FeaturedItem[rects.size()];
        for (int i = 0; i < rects.size(); ++i) {
            aChar[i] = new FeaturedItem(this, trainData, rects.get(i));
        }
//        for (int i = 0; i< 165; ++i)
//        {
//            PImage image = loadImage(i+"_.jpg");
//        }
        blackBox = BlackBox.load(this, "BLE.txt");
        learn();
        //splitImage("train.png", "u", false);
    }


    private void splitImage(String file, String folder, boolean isBlack)
    {
        PImage im = loadImage(file).copy();
        if (!isBlack) Histogram.reverse(this,im);
        List<Rect> rects = BlackDetect.detectRect(this, im,
                15, 100);
        int k = 0;
        for (Rect r: rects)
        {
            im.get(r.left, r.top, r.right-r.left+1, r.button-r.top+1).save(folder+"/"+k+".jpg");
            k++;
        }
    }

    private void learn() {
        int learned = 0;
        for (int i = 0; i < 10; ++i) {
            String folder = "training_data/"+i + "/out/";
            int k = 0;
            while (true) {
                //if (k>300) break;
                String f = folder + k + ".jpg";
                PImage image = loadImage(f);
                if (image == null) break;
                if (image.width < 1) break;
                FeaturedItem featuredItem = new FeaturedItem(this, image);
                if (!blackBox.exist(featuredItem)) {
                    blackBox.learn(featuredItem, i);
                    learned++;
                }
                k++;
            }
        }
        println("Learned " + learned);
        blackBox.save("BLE.txt");
    }

    @Override
    public void draw() {
        //image(trainData, 0, 0);
        background(255);
        if (id >= rects.size()) {
            if (id == rects.size()) {
                //// TODO: 03/01/2017 save Blackbox
            }
        } else {
            image(aChar[id].getImage(), 20, 20);
            aChar[id].virtualization(20, 300);
            stroke(0, 255, 255);
            fill(0, 100, 0);
            textSize(40);
            text("I think it is " + blackBox.query(aChar[id]), 100, 300);
        }
        noFill();
    }

    @Override
    public void keyPressed() {
        if (key >= '0' && key <= '9') {
            blackBox.learn(aChar[id], key - '0');
            id++;

        }
        if (key == 'b') id--;
        if (key == 'n') id++;
        if (key == 's') blackBox.save("BLE.txt");
        if (id < 0) id = 0;
        if (id >= rects.size())
            id = rects.size() - 1;

    }
}
