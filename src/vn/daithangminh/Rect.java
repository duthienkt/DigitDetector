package vn.daithangminh;

import static java.lang.Math.max;
import static java.lang.Math.min;

/**
 * Created by DuThien on 02/01/2017.
 */
public class Rect {
    public final int left, top, button, right;

    public Rect(int left, int top, int right, int button) {
        this.left = left;
        this.top = top;
        this.button = button;
        this.right = right;
    }

    public Rect merge(Rect a2) {
        if (a2 == null) return this;
        return new Rect(min(left, a2.left), min(top, a2.top), max(right, a2.right), max(button, a2.button));
    }

    public boolean isOverlap(Rect a2) {
        if (left > a2.right) return false;
        if (top > a2.button) return false;
        if (right < a2.left) return false;
        if (button < a2.top) return false;
        return true;
    }

    public float SQ() {
        return (right - left + 1) * (button - top + 1);
    }


}
