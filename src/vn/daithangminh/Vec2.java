package vn.daithangminh;

import static com.jogamp.opengl.math.FloatUtil.sqrt;
import static java.lang.Math.PI;
import static java.lang.Math.abs;
import static java.lang.Math.atan2;

/**
 * Created by DuThien on 02/01/2017.
 */
public class Vec2 {
    public final float x, y;

    public Vec2(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public static int distance(int i, int j) {
        int d1 = i - j;
        if (d1 < 0) d1 = -d1;
        int d2 = i > j ? j + 9 : j - 9;
        if (d2 < 0) d2 = -d2;
        return d1 < d2 ? d1 : d2;
    }

    public static float dot(int i, int j)
    {
        return (float) abs(Math.cos(distance(i, j)* Math.PI/9));
    }

    public int getDegreeId() {
        double r = atan2(y, x);
        if (r < 0) r += 2 * PI;
        int res = (int) (r * 9 / PI);
        return res % 9;
    }

    public float length() {
        return (float) sqrt(x * x + y * y);
    }

    public Vec2 add(Vec2 a2) {
        return new Vec2(x + a2.x, y + a2.y);
    }

    public Vec2 sub(Vec2 a2) {
        return new Vec2(x - a2.x, y - a2.y);

    }

    public Vec2 reverse() {
        return new Vec2(-x, -y);
    }

    public Vec2 increase(Vec2 a2) {
        Vec2 v1 = add(a2);
        Vec2 v2 = sub(a2);
        return v1.length() > v2.length() ? v1 : v2;
    }
}
