package vn.daithangminh.minicv;

import processing.core.PApplet;
import processing.core.PImage;
import vn.daithangminh.Rect;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DuThien on 04/01/2017.
 */
public class BlackDetect {

    static int[] dx = new int[]{0, 1, 1, 1, 0, -1, -1, -1};
    static int[] dy = new int[]{-1, -1, 0, 1, 1, 1, 0, -1};

    public static List<Rect> detectRect(PApplet applet, PImage image, int sq_min, int threshold) {
        PImage r = image.copy();
        List<Rect> rects = new ArrayList<>();
        for (int y = 0; y < r.height; ++y)
            for (int x = 0; x < r.width; ++x) {
                Rect rect = visit(applet, r, x, y, threshold);
                if (rect!= null)
                if (rect.SQ() > sq_min)
                    rects.add(rect);
            }
        //remove overlap
        for (int i = 0; i < rects.size(); ++i)
            for (int j = i + 1; j < rects.size(); ++j)
                if (rects.get(i).isOverlap(rects.get(j))) {
                    rects.set(i, rects.get(i).merge(rects.get(j)));
                    rects.remove(j);
                }
        return rects;
    }

    public static Rect visit(PApplet applet, PImage r, int x, int y, int threshold) {
        if (x < 0) return null;
        if (y < 0) return null;
        if (x >= r.width) return null;
        if (y >= r.height) return null;

        int c = r.get(x, y);
        float gr = applet.brightness(c);
        if (gr > threshold) return null;
        r.set(x, y, 0xffffff);
        Rect res = new Rect(x, y, x, y);
        for (int i = 0; i < 8; ++i)
            res = res.merge(visit(applet, r, x + dx[i], y + dy[i], threshold));
        return res;
    }

}
