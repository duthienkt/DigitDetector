package vn.daithangminh.minicv;


import processing.core.PApplet;
import processing.core.PImage;

import java.applet.Applet;

/**
 * Created by DuThien on 04/01/2017.
 */
public class Histogram {
    public final float[] dataS;

    private Histogram(float[] dataS) {
        this.dataS = dataS;
    }

    public static void reverse(PApplet applet, PImage im) {
        for (int y = 0; y < im.height; ++y)
            for (int x = 0; x < im.width; ++x) {
                int c = im.get(x, y);
                int r = applet.color(255 - applet.red(c), 255 - applet.green(c), 255 - applet.blue(c), 255);
                im.set(x, y, r);
            }
    }

    public static Histogram create(PApplet applet, PImage image) {
        int[] c = new int[256];
        for (int i = 0; i < 0; ++i)
            c[i] = 0;
        for (int x = 0; x < image.width; ++x)
            for (int y = 0; y < image.height; ++y) {
                c[(int) applet.brightness(image.get(x, y))]++;
            }
        int sum = image.width * image.height;
        int t = 0;
        final float dataS[] = new float[256];
        for (int i = 0; i < 256; ++i) {
            t += c[i];
            dataS[i] = t / (float)sum;
        }
        return new Histogram(dataS);
    }

    /**
     * @param pr1 form 0 to 1.0f
     * @return
     */

    public int getThreshold(float pr1) {
       //PApplet.println(dataS);
        float dis = 10;
        int res = 0;
        for (int i = 0; i < 256; ++i) {
            if (abs(pr1 - dataS[i]) < dis) {
                res = i;
                dis = abs(pr1 - dataS[i]);
            }
            else break;
        }

        return res;
    }

    private float abs(float x) {
        return x >= 0 ? x : -x;
    }

}
